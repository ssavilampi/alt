#include "globals.h"
#include "ball.h"
#pragma aux bubble1m0 = \
"add ebx, 1024" \
"mov eax, [ebx]  " \
"or eax, 8388608" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble3m0 = \
"add ebx, 960" \
"mov eax, [ebx]  " \
"or eax, 8388864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8388864" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble5m0 = \
"add ebx, 896" \
"mov eax, [ebx]  " \
"or eax, 8388864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12583680" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12583680" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8388864" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble7m0 = \
"add ebx, 832" \
"mov eax, [ebx]  " \
"or eax, 8389376" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12584704" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12584704" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12584704" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8389376" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble9m0 = \
"add ebx, 768" \
"mov eax, [ebx]  " \
"or eax, 8389376" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12584704" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14683904" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14683904" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14683904" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12584704" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8389376" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble11m0 = \
"add ebx, 704" \
"mov eax, [ebx]  " \
"or eax, 8390400" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12586752" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14688000" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14688000" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14688000" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14688000" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12586752" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8390400" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble13m0 = \
"add ebx, 640" \
"mov eax, [ebx]  " \
"or eax, 8390400" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14688000" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14688000" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15744768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15744768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15744768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15744768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14688000" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14688000" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8390400" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble15m0 = \
"add ebx, 576" \
"mov eax, [ebx]  " \
"or eax, 8392448" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12590848" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14696192" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15761152" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15761152" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15761152" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15761152" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15761152" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14696192" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12590848" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8392448" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble17m0 = \
"add ebx, 512" \
"mov eax, [ebx]  " \
"or eax, 8392448" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14696192" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15761152" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15761152" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318208" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318208" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318208" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318208" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318208" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15761152" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15761152" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14696192" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8392448" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble19m0 = \
"add ebx, 448" \
"mov eax, [ebx]  " \
"or eax, 8396544" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12599040" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14712576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793920" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318209" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318209" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318209" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318209" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318209" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318209" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793920" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14712576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12599040" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8396544" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble21m0 = \
"add ebx, 384" \
"mov eax, [ebx]  " \
"or eax, 8396544" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14712576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793920" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318209" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318209" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580355" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580355" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580355" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580355" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580355" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580355" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318209" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318209" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793920" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14712576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8396544" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble23m0 = \
"add ebx, 320" \
"mov eax, [ebx]  " \
"or eax, 7936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12615424" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793921" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793921" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318211" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318211" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580359" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580359" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580359" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580359" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580359" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318211" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318211" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793921" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793921" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12615424" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 7936" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble25m0 = \
"add ebx, 256" \
"mov eax, [ebx]  " \
"or eax, 8404736" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14745344" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793921" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318211" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580359" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580359" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711439" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711439" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711439" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711439" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711439" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711439" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711439" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580359" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580359" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318211" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793921" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14745344" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8404736" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble27m0 = \
"add ebx, 192" \
"mov eax, [ebx]  " \
"or eax, 16128" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12648192" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793923" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318215" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318215" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580367" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580367" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580367" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580367" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318215" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318215" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793923" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12648192" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16128" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble29m0 = \
"add ebx, 128" \
"mov eax, [ebx]  " \
"or eax, 16128" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14745345" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793923" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318215" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580367" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777023" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777023" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777023" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777023" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777023" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777023" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711455" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580367" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318215" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793923" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14745345" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16128" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble31m0 = \
"add ebx, 64" \
"mov eax, [ebx]  " \
"or eax, 32512" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12648193" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793927" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318223" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580383" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580383" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711487" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711487" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777087" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777087" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777087" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777087" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777087" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777087" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16777087" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711487" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711487" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580383" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580383" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16318223" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15793927" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12648193" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 32512" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble1m1 = \
"add ebx, 1024" \
"mov eax, [ebx]  " \
"or eax, 2097152" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble3m1 = \
"add ebx, 960" \
"mov eax, [ebx]  " \
"or eax, 6291456" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 6291456" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble5m1 = \
"add ebx, 896" \
"mov eax, [ebx]  " \
"or eax, 6291456" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15728640" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15728640" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 6291456" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble7m1 = \
"add ebx, 832" \
"mov eax, [ebx]  " \
"or eax, 14680064" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15728896" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15728896" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15728896" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14680064" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble9m1 = \
"add ebx, 768" \
"mov eax, [ebx]  " \
"or eax, 14680064" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15728896" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16253696" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16253696" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16253696" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15728896" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14680064" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble11m1 = \
"add ebx, 704" \
"mov eax, [ebx]  " \
"or eax, 14680320" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15729408" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16254720" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16254720" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16254720" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16254720" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15729408" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14680320" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble13m1 = \
"add ebx, 640" \
"mov eax, [ebx]  " \
"or eax, 14680320" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16254720" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16254720" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16518912" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16518912" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16518912" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16518912" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16254720" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16254720" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14680320" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble15m1 = \
"add ebx, 576" \
"mov eax, [ebx]  " \
"or eax, 14680832" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15730432" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16256768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16523008" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16523008" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16523008" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16523008" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16523008" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16256768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15730432" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14680832" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble17m1 = \
"add ebx, 512" \
"mov eax, [ebx]  " \
"or eax, 14680832" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16256768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16523008" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16523008" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16662272" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16662272" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16662272" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16662272" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16662272" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16523008" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16523008" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16256768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14680832" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble19m1 = \
"add ebx, 448" \
"mov eax, [ebx]  " \
"or eax, 14681856" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15732480" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16260864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16531200" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16678656" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16678656" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16678656" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16678656" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16678656" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16678656" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16531200" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16260864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15732480" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14681856" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble21m1 = \
"add ebx, 384" \
"mov eax, [ebx]  " \
"or eax, 14681856" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16260864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16531200" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16678656" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16678656" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776960" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776960" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776960" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776960" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776960" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776960" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16678656" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16678656" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16531200" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16260864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14681856" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble23m1 = \
"add ebx, 320" \
"mov eax, [ebx]  " \
"or eax, 12584704" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15736576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16547584" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16547584" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711424" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711424" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776961" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776961" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776961" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776961" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776961" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711424" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711424" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16547584" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16547584" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15736576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12584704" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble25m1 = \
"add ebx, 256" \
"mov eax, [ebx]  " \
"or eax, 14683904" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16269056" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16547584" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711424" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776961" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776961" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706685" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706685" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706685" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706685" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706685" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706685" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706685" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776961" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776961" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711424" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16547584" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16269056" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 14683904" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble27m1 = \
"add ebx, 192" \
"mov eax, [ebx]  " \
"or eax, 12586752" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15744768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580352" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711425" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711425" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776963" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776963" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776963" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776963" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711425" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711425" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580352" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15744768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12586752" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble29m1 = \
"add ebx, 128" \
"mov eax, [ebx]  " \
"or eax, 12586752" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16285440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580352" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711425" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776963" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964849" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964849" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964849" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964849" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964849" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964849" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706681" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776963" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711425" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580352" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16285440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12586752" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble31m1 = \
"add ebx, 64" \
"mov eax, [ebx]  " \
"or eax, 12590848" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15761152" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580353" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711427" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776967" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776967" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706673" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706673" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964833" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964833" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964833" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964833" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964833" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964833" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964833" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706673" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706673" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776967" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16776967" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711427" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16580353" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15761152" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 12590848" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble1m2 = \
"add ebx, 1024" \
"mov eax, [ebx]  " \
"or eax, 524288" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble3m2 = \
"add ebx, 960" \
"mov eax, [ebx]  " \
"or eax, 1572864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 1572864" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble5m2 = \
"add ebx, 896" \
"mov eax, [ebx]  " \
"or eax, 1572864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 3932160" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 3932160" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 1572864" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble7m2 = \
"add ebx, 832" \
"mov eax, [ebx]  " \
"or eax, 3670016" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8126464" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8126464" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8126464" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 3670016" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble9m2 = \
"add ebx, 768" \
"mov eax, [ebx]  " \
"or eax, 3670016" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8126464" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646144" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646144" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646144" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8126464" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 3670016" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble11m2 = \
"add ebx, 704" \
"mov eax, [ebx]  " \
"or eax, 7864320" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16515072" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646400" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646400" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646400" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646400" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16515072" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 7864320" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble13m2 = \
"add ebx, 640" \
"mov eax, [ebx]  " \
"or eax, 7864320" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646400" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646400" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16712448" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16712448" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16712448" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16712448" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646400" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646400" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 7864320" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble15m2 = \
"add ebx, 576" \
"mov eax, [ebx]  " \
"or eax, 16252928" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16515328" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646912" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16713472" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16713472" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16713472" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16713472" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16713472" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646912" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16515328" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16252928" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble17m2 = \
"add ebx, 512" \
"mov eax, [ebx]  " \
"or eax, 16252928" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646912" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16713472" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16713472" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130768128" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130768128" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130768128" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130768128" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130768128" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16713472" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16713472" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646912" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16252928" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble19m2 = \
"add ebx, 448" \
"mov eax, [ebx]  " \
"or eax, 16253184" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16515840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16647936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16715520" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130764032" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130764032" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130764032" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130764032" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130764032" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130764032" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16715520" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16647936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16515840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16253184" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble21m2 = \
"add ebx, 384" \
"mov eax, [ebx]  " \
"or eax, 16253184" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16647936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16715520" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130764032" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130764032" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057014016" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057014016" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057014016" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057014016" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057014016" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057014016" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130764032" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130764032" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16715520" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16647936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16253184" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble23m2 = \
"add ebx, 320" \
"mov eax, [ebx]  " \
"or eax, 15728896" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16516864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16719616" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16719616" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130755840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130755840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056997632" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056997632" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056997632" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056997632" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056997632" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130755840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130755840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16719616" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16719616" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16516864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15728896" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble25m2 = \
"add ebx, 256" \
"mov eax, [ebx]  " \
"or eax, 16253696" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16649984" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16719616" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130755840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056997632" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056997632" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093952" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093952" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093952" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093952" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093952" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093952" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093952" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056997632" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056997632" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130755840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16719616" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16649984" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16253696" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble27m2 = \
"add ebx, 192" \
"mov eax, [ebx]  " \
"or eax, 15729408" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16518912" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16727808" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130739456" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130739456" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130739456" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130739456" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16727808" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16518912" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15729408" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble29m2 = \
"add ebx, 128" \
"mov eax, [ebx]  " \
"or eax, 15729408" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16654080" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16727808" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130739456" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658493" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658493" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658493" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658493" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658493" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658493" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093951" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964864" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130739456" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16727808" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16654080" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15729408" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble31m2 = \
"add ebx, 64" \
"mov eax, [ebx]  " \
"or eax, 15730432" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16523008" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16744192" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706688" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964863" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964863" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093949" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093949" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658489" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658489" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658489" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658489" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658489" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658489" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251658489" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093949" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520093949" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964863" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1056964863" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130706688" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16744192" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16523008" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 15730432" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble1m3 = \
"add ebx, 1024" \
"mov eax, [ebx]  " \
"or eax, 131072" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble3m3 = \
"add ebx, 960" \
"mov eax, [ebx]  " \
"or eax, 393216" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 393216" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble5m3 = \
"add ebx, 896" \
"mov eax, [ebx]  " \
"or eax, 393216" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 983040" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 983040" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 393216" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble7m3 = \
"add ebx, 832" \
"mov eax, [ebx]  " \
"or eax, 917504" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 2031616" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 2031616" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 2031616" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 917504" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble9m3 = \
"add ebx, 768" \
"mov eax, [ebx]  " \
"or eax, 917504" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 2031616" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2143354880" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2143354880" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2143354880" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 2031616" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 917504" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble11m3 = \
"add ebx, 704" \
"mov eax, [ebx]  " \
"or eax, 1966080" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 4128768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2139160576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2139160576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2139160576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2139160576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 4128768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 1966080" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble13m3 = \
"add ebx, 640" \
"mov eax, [ebx]  " \
"or eax, 1966080" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2139160576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2139160576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057030144" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057030144" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057030144" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057030144" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2139160576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2139160576" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 1966080" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble15m3 = \
"add ebx, 576" \
"mov eax, [ebx]  " \
"or eax, 4063232" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8323072" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130771968" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029888" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029888" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029888" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029888" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029888" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130771968" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8323072" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 4063232" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble17m3 = \
"add ebx, 512" \
"mov eax, [ebx]  " \
"or eax, 4063232" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130771968" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029888" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029888" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520158464" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520158464" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520158464" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520158464" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520158464" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029888" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029888" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130771968" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 4063232" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble19m3 = \
"add ebx, 448" \
"mov eax, [ebx]  " \
"or eax, 8257536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711680" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130771712" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029376" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520157440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520157440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520157440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520157440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520157440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520157440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029376" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130771712" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711680" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8257536" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble21m3 = \
"add ebx, 384" \
"mov eax, [ebx]  " \
"or eax, 8257536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130771712" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029376" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520157440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520157440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251719936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251719936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251719936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251719936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251719936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251719936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520157440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520157440" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057029376" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130771712" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8257536" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble23m3 = \
"add ebx, 320" \
"mov eax, [ebx]  " \
"or eax, 8126464" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057028352" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057028352" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520155392" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520155392" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251715840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251715840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251715840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251715840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251715840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520155392" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520155392" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057028352" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057028352" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16711936" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 8126464" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble25m3 = \
"add ebx, 256" \
"mov eax, [ebx]  " \
"or eax, 16646144" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130771200" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057028352" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520155392" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251715840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251715840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117489920" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117489920" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117489920" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117489920" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117489920" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117489920" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117489920" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251715840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251715840" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520155392" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057028352" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130771200" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16646144" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble27m3 = \
"add ebx, 192" \
"mov eax, [ebx]  " \
"or eax, 16515072" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16712448" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057026304" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520151296" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520151296" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251707648" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251707648" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251707648" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251707648" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520151296" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520151296" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057026304" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16712448" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16515072" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble29m3 = \
"add ebx, 128" \
"mov eax, [ebx]  " \
"or eax, 16515072" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130770176" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057026304" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520151296" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251707648" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331904" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331904" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331904" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331904" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331904" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331904" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117473536" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251707648" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520151296" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057026304" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -2130770176" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16515072" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
#pragma aux bubble31m3 = \
"add ebx, 64" \
"mov eax, [ebx]  " \
"or eax, 16515328" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16713472" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057022208" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520143104" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251691264" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251691264" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117440768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117440768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331903" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331903" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331903" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331903" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331903" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331903" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -50331903" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117440768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -117440768" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251691264" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -251691264" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -520143104" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, -1057022208" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16713472" \
"mov [ebx], eax" \
"add ebx, 64 " \
"mov eax, [ebx]  " \
"or eax, 16515328" \
"mov [ebx], eax" \
"add ebx, 64 " \
parm [ebx] \
modify [eax];
void draw_ball( word x, word y, word size, word plane) {
  word a = x & 3;
  long address = (bplane[plane]) + (long)((x >> 2) + (y << 6));
  switch( (size << 2) + a) {
  case 0: 
    bubble1m0( address);
    break;
  case 4: 
    bubble3m0( address);
    break;
  case 8: 
    bubble5m0( address);
    break;
  case 12: 
    bubble7m0( address);
    break;
  case 16: 
    bubble9m0( address);
    break;
  case 20: 
    bubble11m0( address);
    break;
  case 24: 
    bubble13m0( address);
    break;
  case 28: 
    bubble15m0( address);
    break;
  case 32: 
    bubble17m0( address);
    break;
  case 36: 
    bubble19m0( address);
    break;
  case 40: 
    bubble21m0( address);
    break;
  case 44: 
    bubble23m0( address);
    break;
  case 48: 
    bubble25m0( address);
    break;
  case 52: 
    bubble27m0( address);
    break;
  case 56: 
    bubble29m0( address);
    break;
  case 60: 
    bubble31m0( address);
    break;
  case 1: 
    bubble1m1( address);
    break;
  case 5: 
    bubble3m1( address);
    break;
  case 9: 
    bubble5m1( address);
    break;
  case 13: 
    bubble7m1( address);
    break;
  case 17: 
    bubble9m1( address);
    break;
  case 21: 
    bubble11m1( address);
    break;
  case 25: 
    bubble13m1( address);
    break;
  case 29: 
    bubble15m1( address);
    break;
  case 33: 
    bubble17m1( address);
    break;
  case 37: 
    bubble19m1( address);
    break;
  case 41: 
    bubble21m1( address);
    break;
  case 45: 
    bubble23m1( address);
    break;
  case 49: 
    bubble25m1( address);
    break;
  case 53: 
    bubble27m1( address);
    break;
  case 57: 
    bubble29m1( address);
    break;
  case 61: 
    bubble31m1( address);
    break;
  case 2: 
    bubble1m2( address);
    break;
  case 6: 
    bubble3m2( address);
    break;
  case 10: 
    bubble5m2( address);
    break;
  case 14: 
    bubble7m2( address);
    break;
  case 18: 
    bubble9m2( address);
    break;
  case 22: 
    bubble11m2( address);
    break;
  case 26: 
    bubble13m2( address);
    break;
  case 30: 
    bubble15m2( address);
    break;
  case 34: 
    bubble17m2( address);
    break;
  case 38: 
    bubble19m2( address);
    break;
  case 42: 
    bubble21m2( address);
    break;
  case 46: 
    bubble23m2( address);
    break;
  case 50: 
    bubble25m2( address);
    break;
  case 54: 
    bubble27m2( address);
    break;
  case 58: 
    bubble29m2( address);
    break;
  case 62: 
    bubble31m2( address);
    break;
  case 3: 
    bubble1m3( address);
    break;
  case 7: 
    bubble3m3( address);
    break;
  case 11: 
    bubble5m3( address);
    break;
  case 15: 
    bubble7m3( address);
    break;
  case 19: 
    bubble9m3( address);
    break;
  case 23: 
    bubble11m3( address);
    break;
  case 27: 
    bubble13m3( address);
    break;
  case 31: 
    bubble15m3( address);
    break;
  case 35: 
    bubble17m3( address);
    break;
  case 39: 
    bubble19m3( address);
    break;
  case 43: 
    bubble21m3( address);
    break;
  case 47: 
    bubble23m3( address);
    break;
  case 51: 
    bubble25m3( address);
    break;
  case 55: 
    bubble27m3( address);
    break;
  case 59: 
    bubble29m3( address);
    break;
  case 63: 
    bubble31m3( address);
    break;
  }
}
