#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <math.h>
#include <conio.h>
#include <malloc.h>
#include <i86.h>
#include <string.h>
#include <time.h>
#include "globals.h"
#include "init.h"
#include "ball.h"
#include "effect1.h"
void effect1_init()
{
    word xx;
    for( xx = 0; xx<2000; xx++)
    {
        ball_pos[xx*3 + 0] = (rand()%250)<<8;
        ball_pos[xx*3 + 1] = (rand()%250)<<8;
        ball_pos[xx*3 + 2] = rand()&15;
    }
}
void effect1()
{
    word xx, yy, pplane;
	for( xx = 0; xx<2000; xx++)
    {
        if( ball_pos[xx*3 + 0] < (256<<8) )  ball_pos[xx*3 + 0] += ((ball_pos[xx*3 + 2]<<4) + 10) * passed_time/2;
        else ball_pos[xx*3 + 0] = ((ball_pos[xx*3 + 2]<<4) + 10) * passed_time;
        ball_pos[xx*3 + 1] += ((ball_pos[xx*3 + 2]<<4) + 10) * passed_time;
    }
    for( xx = 0; xx<2000; xx++)
        draw_ball( ball_pos[xx*3 + 0]>>8, ball_pos[xx*3 + 1]>>8, ball_pos[xx*3 + 2], ball_pos[xx*3 + 2]>>2 );

}