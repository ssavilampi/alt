#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <math.h>
#include <conio.h>
#include <malloc.h>
#include <i86.h>
#include <string.h>
#include <time.h>
#include "globals.h"
#include "init.h"
#include "ball.h"
#include "effect2.h"
void effect2_init()
{
    word xx;
    int i;
    for( i = 0; i<4000; i++)
    {
        dots[i][0] = sin(i)*0.85f;
        dots[i][1] = cos(i)*0.85f;
        dots[i][2] = (rand()%4096)/4096.0f;
    }

    for( i = 0; i<4000; i++)
    {
        ball_pos[i*3 + 2] = (int)dots[i][2]*15;
        ball_pos[i*3 + 0] = (word)(70 + 70*dots[i][0]/(dots[i][2]*25.0f)) <<8;
        ball_pos[i*3 + 1] = (word)(125 + 100*dots[i][1]/(dots[i][2]*25.0f)) <<8;
    }
}
void effect2()
{
    word xx, yy, pplane;
    int i;
    float passer = passed_time;
    for( i = 0; i<4000; i++)
    {
        if(dots[i][2]>0.2f) dots[i][2] -= 0.006f;
        else dots[i][2] = 1.0f;
        dots[i][0] = sin(i+timenow*0.01f)*0.85f*(0.8f+0.6f*sin(dots[i][2]*4.0f+timenow*0.00121012f));
        dots[i][1] = cos(i+timenow*0.01f)*0.85f*(0.8f+0.6f*sin(dots[i][2]*4.0f+timenow*0.00121f));
        ball_pos[i*3 + 2] = (int)(4.5f-dots[i][2]*5.0f);
        ball_pos[i*3 + 0] = (word)(70 + 10*dots[i][0]/(dots[i][2]*0.7f)) <<8;
        ball_pos[i*3 + 1] = (word)(125 + 20*dots[i][1]/(dots[i][2]*0.7f)) <<8;
    }
    for( xx = 0; xx<4000; xx++)
        draw_ball( ball_pos[xx*3 + 0]>>8, ball_pos[xx*3 + 1]>>8, ball_pos[xx*3 + 2], ball_pos[xx*3 + 2] );

}