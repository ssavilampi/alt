#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <math.h>
#include <conio.h>
#include <malloc.h>
#include <i86.h>
#include <string.h>
#include <time.h>
#include "globals.h"
#include "init.h"
#include "ball.h"
#include "effect4.h"
void effect4_init()
{
    word xx;
    int i;
    for( i = 0; i<4000; i++)
    {
        dots[i][0] = 0;
        dots[i][1] = 1.0f;
        dots[i][2] = 0;
    }

    for( i = 0; i<4000; i++)
    {
        ball_pos[i*3 + 2] = (int)dots[i][2]*15;
        ball_pos[i*3 + 0] = (word)(70 + 70*dots[i][0]/(dots[i][2]*25.0f)) <<8;
        ball_pos[i*3 + 1] = (word)(125 + 100*dots[i][1]/(dots[i][2]*25.0f)) <<8;
    }
}
void effect4()
{
    int i;
    for( i = 0; i<4000; i++)
    {
        dots[i][0]=sin(i*0.01f+timenow*0.1f)*sin((i*0.01f+timenow*0.7f)*0.1f);
        dots[i][1] = 1.5f-(i)/1500.0f;
        dots[i][2]=0.5f+0.3f*cos(i*0.01f+timenow*0.1f)*sin((i*0.01f+timenow*0.7f)*0.1f);
        ball_pos[i*3 + 2] = (int)(4.5f-dots[i][2]*5.0f);
        ball_pos[i*3 + 0] = (word)(70 + 10*dots[i][0]/(dots[i][2]*0.7f)) <<8;
        ball_pos[i*3 + 1] = (word)(125 + 20*dots[i][1]/(dots[i][2]*0.7f)) <<8;
        if(ball_pos[i*3 + 1] > (250<<8) ) ball_pos[i*3 + 1] = 0;
        if(ball_pos[i*3 + 1] < 0 ) ball_pos[i*3 + 1] = 0;
    }
    for( i = 0; i<4000; i++)
        draw_ball( ball_pos[i*3 + 0]>>8, ball_pos[i*3 + 1]>>8, 1+ball_pos[i*3 + 2], ball_pos[i*3 + 2] );

}
void effect4out()
{
    int i;
    for( i = 0; i<4000; i++)
    {
        dots[i][0] /= 0.98f;
        if(dots[i][0]<0.01f && dots[i][0]>-0.01f) dots[i][0]=sin(i*0.01f+timenow*0.1f)*sin((i*0.01f+timenow*0.7f)*0.1f);
        dots[i][1] = 1.5f-(i)/1500.0f;
        dots[i][2] /= 0.98f;
        if(dots[i][2]<0.01f && dots[i][2]>-0.01f) dots[i][2]=0.5f+0.3f*cos(i*0.01f+timenow*0.1f)*sin((i*0.01f+timenow*0.7f)*0.1f);
        ball_pos[i*3 + 2] = (int)(4.5f-dots[i][2]*5.0f);
        ball_pos[i*3 + 0] = (word)(70 + 10*dots[i][0]/(dots[i][2]*0.7f)) <<8;
        ball_pos[i*3 + 1] = (word)(125 + 20*dots[i][1]/(dots[i][2]*0.7f)) <<8;
        if(ball_pos[i*3 + 0] > (256<<8) ) ball_pos[i*3 + 0] = 0;
        if(ball_pos[i*3 + 1] > (250<<8) ) ball_pos[i*3 + 1] = 0;
        if(ball_pos[i*3 + 2] > 15 ) ball_pos[i*3 + 2] = 0;
    }
    for( i = 0; i<4000; i++)
        draw_ball( ball_pos[i*3 + 0]>>8, ball_pos[i*3 + 1]>>8, 1+ball_pos[i*3 + 2], ball_pos[i*3 + 2] );

}