#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <math.h>
#include <conio.h>
#include <malloc.h>
#include <i86.h>
#include <string.h>
#include <time.h>
#include "globals.h"
#include "init.h"
#include "ball.h"
#include "effect6.h"
void effect6_init()
{
    word xx;
    int i;
    for( i = 0; i<4000; i++)
    {
        dots[i][0] = cos(i&255)*sin(i>>8);
        dots[i][1] = sin(i&255)*sin(i>>8);
        dots[i][2] = 0.5f+cos(i)*0.4f;
    }

    for( i = 0; i<4000; i++)
    {
        ball_pos[i*3 + 2] = (int)dots[i][2]*15;
        ball_pos[i*3 + 0] = (word)(70 + 70*dots[i][0]/(dots[i][2]*25.0f)) <<8;
        ball_pos[i*3 + 1] = (word)(125 + 100*dots[i][1]/(dots[i][2]*25.0f)) <<8;
    }
}
void effect6()
{
    int i;
    for( i = 0; i<4000; i++)
    {
        dots[i][2] = 0.5f+cos(i+timenow*0.01f)*0.4f;
        ball_pos[i*3 + 2] = (int)(4.5f-dots[i][2]*3.0f);
        ball_pos[i*3 + 0] = (word)(70 + 5*dots[i][0]/(dots[i][2]*0.21f)) <<8;
        ball_pos[i*3 + 1] = (word)(125 + 10*dots[i][1]/(dots[i][2]*0.21f)) <<8;
        if(ball_pos[i*3 + 0] > (256<<8) ) ball_pos[i*3 + 0] = 0;
        if(ball_pos[i*3 + 1] > (250<<8) ) ball_pos[i*3 + 1] = 0;
        if(ball_pos[i*3 + 2] > 15 ) ball_pos[i*3 + 2] = 0;
    }
    for( i = 0; i<4000; i++)
        draw_ball( ball_pos[i*3 + 0]>>8, ball_pos[i*3 + 1]>>8, 1+ball_pos[i*3 + 2], ball_pos[i*3 + 2]);

}