#ifndef GLOBALS_H_
#define GLOBALS_H_

/*tässä määritellään joitain yleisiä vakiota joita tulee noudattaa*/
#define SCREEN_WIDTH      320
#define SCREEN_HEIGHT     200
#define BUFFER_WIDTH      SCREEN_WIDTH*2
#define BUFFER_HEIGHT     SCREEN_HEIGHT*2
#define Swap(X,Y)  do{ __typeof__ (X) _T = X; X = Y; Y = _T; }while(0)
#define SwapXOR(X,Y)  do{ __typeof__ (X) X ^= Ŷ; Y ^= X;  X ^= Y; }while(0)
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

/*tässä on osoitteita I/O:hon ja muistirakenteisiin*/
#define INPUT_STATUS_1          0x03da      //tilatekisteri( sisältää v ja hsynkin )
#define VRETRACE                0x08      //vsynkin bitti
#define SEQUENCE_CONTROLLER   0x03c4      //sekvenssin ohjain
#define SEQUENCE_DATA     0x03c5      //-||- data
#define CRTC_INDEX        0x03d4      //CRT ohjain
#define CRTC_DATA       0x03d5      //CRT data
#define VRAM          0xA0000   //videomuisti
#define PI 3.14159
/*tyyppien yksinkertaistamista*/
typedef unsigned char  byte;					//tavu
typedef unsigned short word;					//sana
typedef unsigned long  dword;       			//pitkä sana

typedef struct {
   float x,y,z;
}vec3;
typedef struct {
   float x,y,z;
   byte c;
}vec3c;
typedef struct {
   float x,y;
}vec2;
typedef struct {
   short x,y,z;
}shortvec3;
typedef struct {
   short x,y;
}shortvec2;
typedef struct {
   vec3 point[6000];
   int vert[2000];
   int np;
   int nv;
}object;

/*tarkkoihin muistiosoitteisiin osoittavia osoitinmuuttujia
  ( huhels kolme osoitin sanaa )*/
extern byte *VGA;						//osoittaa videomuistiin, luetaan tavuissa
extern word *clk;				//osoittaa 12.5hz kelloon

/*pari framebufferia, olethan tarkkana näiden kanssa*/
extern byte buffer[ SCREEN_HEIGHT * SCREEN_WIDTH ];
extern byte bplane[4][ 64 * 300 ];
extern word ball_pos[8000*3];
extern float dots[8000][3];

extern word timenow;                 //kauan ohjelma on pyörinyt
extern word frameCount;
extern word oldCount;
extern word start;    
extern word passed_time;
#endif