#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <math.h>
#include <conio.h>
#include <malloc.h>
#include <i86.h>
#include <string.h>
#include <time.h>
#include "globals.h"
#include "init.h"
//vaihdetaan videotilaa
void set_mode( byte mode )
{
  union REGS regs;

  regs.h.ah = 0x00;
  regs.h.al = mode;
  int386( 0x10, &regs, &regs );
}
//ruudun leveys
void screenwidth( byte width )
{
        outpw( CRTC_INDEX, 0x05 );
        outpw( CRTC_DATA, width );
}
//ruudun korkeus
void screenheight( byte height )
{
        outpw( CRTC_INDEX, 0x12 );
        outpw( CRTC_DATA, height );
}
//aloitetaan kello, vga, siemennetään random
void begin()
{
  int b;
  srand( *clk );        //randomin siemennys
  set_mode( 0x13 );
  outp( 0x03c8, 0 );
  for ( b = 0; b < 15; b++ )
  {
    outp( 0x03c9, (b<<2)+3 );
    outp( 0x03c9, b+2 );
    outp( 0x03c9, (b<<1)+1 );
  }
  for( b = 15; b < 256; b++ )
  {
    outp( 0x03c9, 255 );
    outp( 0x03c9, 255 );
    outp( 0x03c9, 255 );
  }
}
void end()
{
  ////////////MIDASstopBackgroundPlay();
  //////MIDASremoveTimerCallbacks();
  //////MIDASclose();
  set_mode( 0x03 );                                                             //paluu tekstitilaan
}
