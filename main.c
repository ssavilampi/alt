#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <math.h>
#include <conio.h>
#include <malloc.h>
#include <i86.h>
#include <string.h>
#include <time.h>
#include "globals.h"
#include "init.h"
#include "ball.h"
#include "effect1.h"
#include "effect2.h"
#include "effect3.h"
#include "effect4.h"
#include "effect5.h"
#define CLOCK12_5HZ       0x046c      //12.5hz kello
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


void testy( long);      //c32 color, where
/* framecountteri */
word frameCount;
word oldCount;
byte *VGA=(byte*)0xA0000L;
byte bplane[4][64 * 300];
word *clk = ( word* )CLOCK12_5HZ;							//osoittaa 12.5hz kelloon
word timenow;												//kauan ohjelma on pyörinyt
byte run=0;													//paljonko kello antoi arvoksi ohjelman alussa
byte buffer1[ BUFFER_HEIGHT * BUFFER_WIDTH ];
byte done = 0;
word start;    
word passed_time;
word ball_pos[8000*3];
float dots[8000][3];
int main()
{
    float i = 0.0f;
    float r,g,b;
    word xx, yy, pplane;
    int tyyppi = 0;
    srand (time(NULL));
    effect1_init();
    set_ega_planar();
    passed_time = timenow = start = *clk;
    while(done == 0)
    {
        timenow = *clk - start;
        passed_time -= timenow;
        while(kbhit())
        {
          switch(getch())
          {
            case 27:
              done = 1;
              break;
          }
        }
        
        r = sin(timenow*0.016231f)*10;
        g = sin(timenow*0.011f)*10;
        b = sin(timenow*0.013712f)*10;
        if (r<0) r=-r;
        if (g<0) g=-g;
        if (b<0) b=-b;
        outp( 0x03c8, 0 );
        for ( xx = 0; xx < 256; xx++)
        {
            outp( 0x03c9, 2+sin(xx*0.03311f)*32+(int)r);
            outp( 0x03c9, 2+sin(xx*0.021f)*32+(int)g);
            outp( 0x03c9, 2+sin(xx*0.0311f)*32+(int)b);
        }
        if(timenow>100 && tyyppi == 0)
        {
            tyyppi=1;
            effect2_init();
        }
        if(timenow>300 && tyyppi == 1)
        {
            tyyppi=2;
            effect3_init();
        }
        if(timenow>400 && tyyppi == 2)
        {
            tyyppi=3;
            effect4_init();
        }
        if(timenow>500 && tyyppi == 3)
        {
            tyyppi=4;
            effect5_init();
        }
        if(timenow>600 && tyyppi == 4)
        {
            tyyppi=5;
            effect6_init();
        }
        if ( tyyppi==1)
            effect2();
        else if ( tyyppi==0)
            effect1();
        else if ( tyyppi==2)
            effect3();
        else if ( tyyppi==3)
            effect4out();
        else if ( tyyppi==4)
            effect5();
        else if ( tyyppi==5)
            effect6();
        for(pplane = 0; pplane < 4; pplane++)
        {
            outp(0x3c4, 0x02);
            outp(0x3c5, 1 << pplane);
            for(yy = 0; yy < 200; yy++)
                for(xx = 0; xx < 40; xx++)
                    VGA[xx + (yy*40)] = bplane[pplane][xx + ((yy+50)<<6)];
        }
        for(pplane = 0; pplane < 4; pplane++)
            for(yy = 0; yy < 300; yy++)
                clearBUFFER( (long)&bplane[pplane][yy<<6] );
        while  ((inp(0x03da) & VRETRACE));
        /* wait until done refreshing */
         while (!(inp(0x03da) & VRETRACE));
        passed_time = timenow;
    }
    set_mode( 0x03 );
	return 0;
}
void set_ega_planar()
{
	set_mode( 0x0d);
}
void clearBUFFER( long);
#pragma aux clearBUFFER = \
    "cli" \
    "xchg esp,ebx" \
    "mov eax, 0" \
    "mov ecx, 2000" \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "push eax" /* 1*/ \
    "push eax" /* 2*/ \
    "push eax" /* 3*/ \
    "push eax" /* 4*/ \
    "xchg esp,ebx" \
    "sti" \
    parm [ebx] \
    modify [eax ecx];